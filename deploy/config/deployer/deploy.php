<?php
namespace Deployer;

require __DIR__ . '/vendor/deployer/deployer/recipe/common.php';
require __DIR__ . '/vendor/deployer/deployer/contrib/rsync.php';

$sharedDirs = json_decode(
    file_get_contents(__DIR__ . '/.deploy/config/shared_dirs.json'),
    true,
    512,
    JSON_THROW_ON_ERROR
);
$writableDirs = json_decode(
    file_get_contents(__DIR__ . '/.deploy/config/writable_dirs.json'),
    true,
    512,
    JSON_THROW_ON_ERROR
);
$clearPaths = json_decode
    file_get_contents(__DIR__ . '/.deploy/config/clear_paths.json'),
    true,
    512,
    JSON_THROW_ON_ERROR
);
$rsyncConfig = json_decode(
    file_get_contents(__DIR__ . '/.deploy/config/rsync.json'),
    true,
    512,
    JSON_THROW_ON_ERROR);

import('.deploy/config/hosts.yml');

set('application', 'My Application Name');

set('repository', 'git@gitlab.int.skygate.de:my-group/my-repo.git');

set('git_tty', true);
set('default_timeout', null);

set('shared_dirs', $sharedDirs);
set('writable_dirs', $writableDirs);
set('clear_paths', $clearPaths);
set('rsync', $rsyncConfig);

desc('Rsync from CI pipeline');
task(
    'deploy:rsync',
    static function () {
        set('rsync_src', __DIR__);
        invoke('rsync');
    }
);

desc('Rsync from CI pipeline');
task(
    'deploy:rsync',
    static function () {
        set('rsync_src', __DIR__);
        invoke('rsync');
    }
);

desc('Deploy from within CI pipeline');
task(
    'deploy',
    [
        'deploy:info',
        'deploy:lock',
        'deploy:release',
        'deploy:rsync',
        'deploy:vendors',
        'deploy:shared',
        'deploy:symlink',
        'deploy:unlock',
        'deploy:clear_paths',
        'deploy:cleanup',
        'deploy:success',
    ]
);

after('deploy:failed', 'deploy:unlock');