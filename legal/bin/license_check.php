#!/usr/bin/env php
<?php

$pathToComposer = getenv('PATH_TO_COMPOSER');
$pathToExceptions = getenv('PATH_TO_LICENSE_EXCEPTIONS') ?: '';
$output = [];

exec($pathToComposer . ' licenses --format=json --no-dev', $output);

$licenseInfoJson = implode($output);

try {
    $licenseInfo = json_decode($licenseInfoJson, true, 512, JSON_THROW_ON_ERROR);
} catch (JsonException $e) {
    echo "\e[0;31mThere was an error json-decoding the license info.\e[0m\n";
    exit(2);
}

try {
    $exceptions = !empty($pathToExceptions)
        ? json_decode(file_get_contents($pathToExceptions), true, 512, JSON_THROW_ON_ERROR)
        : [];
} catch (JsonException $e) {
    echo "\e[0;31mThere was an error json-decoding your package exceptions:\e[0m\nPlease make sure "
        . $pathToExceptions . " contains valid JSON.\n";
    exit(3);
}

$licenseWhitelist = [
    'MIT',
    'BSD-2',
    'BSD-3',
    'Apache-2.0'
];

$packagesWithLicenseIssues = [];

$pattern = '/^(' . implode('|', $licenseWhitelist) . ')/';

foreach ($licenseInfo['dependencies'] as $package => $info) {
    $license = (string)current($info['license']);
    if (!preg_match($pattern, $license) && !in_array($package, $exceptions, true)) {
        $packagesWithLicenseIssues[] = $package . ' (' . ($license ?: 'none') . ')';
    }
}

if (empty($packagesWithLicenseIssues)) {
    echo "\e[0;32mNo license issues found. Good work!\e[0m\n";
    exit(0);
}

echo "\e[0;33mThe following packages have license issues, please check:\e[0m\n";

foreach ($packagesWithLicenseIssues as $package) {
    echo $package . "\n";
}

exit(1);
