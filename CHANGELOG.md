# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Fixed:
- mv command in template php_compat_phar
### Changed:
- Replaced template symfony_security_check by php_security_check in gitlab ci dist files
- Replaced SensioLabs Security Checker by Local PHP Security Checker

## [1.0.0] - 2020-12-11
