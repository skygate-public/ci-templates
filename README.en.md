# SkyGate CI Template Repository

This repository provides CI templates that can be used by SkyGate developers both for SkyGate internal projects as well as projects for customers. 

They can be easily referenced from repositories hosted on the __SkyGate GitLab__ via template include. 

Repositories that are hosted at __gitlab.com__ can also include them via template include - from a mirrored, public repository at https://gitlab.com/skygate-public/ci-templates/.

Other external repositories can still use them by copying its contents.

## Repository Structure

You will find the templates organized in folders by topic. Any configuration files needed to run the specified jobs are available in a sub-folder called `config` inside the topic folder. 

## Using a Job Template via Template Include (for repositories within the SkyGate GitLab or at gitlab.com)

First of all, you will need a GitLab CI pipeline config file (`.gitlab-ci.yml`), so if you do not already have one set up for your project, make sure to create it now. Let's say you want to use a job called `sample-job` that's defined in `code_quality/example.yml`. 

1. Include the template that defines the job you would like to use in your `.gitlab-ci.yml`:

       // Repository at SkyGate GitLab:
       include:
          - project: 'skygate/ci-templates'
            ref: 'master'
            file: 'code_quality/example.yml'
            
       // Repository at gitlab.com:
       include:
          - project: 'skygate-public/ci-templates'
            ref: 'master'
            file: 'code_quality/example.yml'

2. Extend the job from the template to assign it to your stage of choice:

       my-project-job:
          extends: .sample-job
          stage: my-stage

3. (Optional) Override variable(s) defined by a job:

       my-project-job:
          ...
          variables:
              JOB_VARIABLE: 'my custom value'


### Using a specific version of a template

If your project requires a specific version of a template you can specify the tag in the 'ref' attribute. 

Both templates from master:

       include:
           - project: 'skygate/ci-templates'
             ref: 'master'
             file:
               - 'code_quality/example_1.yml'
               - 'code_quality/example_2.yml'

Different versions:

       include:
           - project: 'skygate/ci-templates'
             ref: 'v1.2'
             file: 'code_quality/example_1.yml'
           - project: 'skygate/ci-templates'
             ref: 'v2.0'
             file: 'code_quality/example_2.yml'

## Provided Job Templates

### Code Style

This section includes job templates to check the correct syntax of source code as well as compliance to coding standards. 

#### PHP CodeSniffer

This is a very basic check for correct PHP syntax and compliance of PSR-12 standard. It should be included in every PHP based project.

- Template: `code_style/code-sniffer.yml`
- Jobs: `.code_sniffer_phar`, `.code_sniffer_vendor`
- Variables: `PATHS_TO_SOURCES`, `PHPCS_VERSION`, `PATH_TO_CACHE`
- Configuration Files: `code_style/config/phpcs.xml.dist`


You can choose between a phar-based version of the job that downloads a fresh copy of the latest PHP CodeSniffer PHAR (`.code_sniffer_phar`) and a vendor-based version of the job in case you have already included PHP CodeSniffer in your project's vendors (`.code_sniffer_vendor`). If using the vendor-based version, please make sure to have your vendors available when running the job (run `composer install` in the `before_script` or make use of caching).

The phar-based version uses the latest release of PHP CodeSniffer by default. If you want to use a different version you can specify the version by overriding the variable `PHPCS_VERSION` (e.g. '3.5.8').

Both versions require a `phpcs.xml` config file located at your project's root directory. The `phpcs.xml.dist` provided in the `config` folder will suit most projects, so just go ahead and copy it to your project's root directory for a start.

If you run your pipeline now, things should already work and print a report to the `stdout` of your job. That is, if all of your PHP source files are located at a directory called `src`. If all or some of your source files are located somewhere else, you can override the variable `PATHS_TO_SOURCES` in your job definition and list all directories to be checked seperated by a single space.

CodeSniffer uses a cache to speed up the analysis. The path to the cache dir can be set by overriding the variable `PATH_TO_CACHE`. By default the path to the cache is `var/.phpcs-cache`. Make sure the path exists in your project.

Note for Symfony based projects: Because the standard cache path is located at `var`, which is ignored by default, you need to add the following lines to your `.gitignore`:

    !var/ 
    var/*
    !var/.gitkeep

Also, you have to create a `.gitkeep` file in your `var` directory.

#### PHP Compatibility Check

This script checks your code for compatibility with a specific PHP version. It should be included in every project as soon as you plan an update of the PHP version.

- Template: `code_style/php-compat.yml`
- Jobs: `.php_compat_phar`, `.php_compat_vendor`
- Variables: `PATHS_TO_SOURCES`, `PHPCS_VERSION`, `PATH_TO_CACHE`, `PHP_VERSION`
- Configuration Files: `code_style/config/phpcs.xml.dist`

You can also choose between a phar-based and a vendor-based version of the job here. The jobs are based on PHP CodeSniffer, so you can use the same configuration options here as for the job "PHP CodeSniffer" (s.a.).

For the phar-based job (`.php_compat_phar`), both CodeSniffer and the necessary standard "PHPCompatiblity" will be downloaded.

The vendor-based version (`.php_compat_vendor`) requires that standard to be installed in your vendors and registered with CodeSniffer. You can find further infos from the [PHP Compatibility Docs](https://github.com/PHPCompatibility/PHPCompatibility#installation-in-a-composer-project-method-1).

The PHP version that your code will be checked against defaults to PHP 8.0. If you want to check against another version you can define it by overriding the variable `PHP_VERSION`.

---

### Code Quality

This section includes job templates to check the quality of source code including semantics, bugs, suboptimal code, unused parameters, methods and properties, etc. 

#### PHPStan

This is a static analysis to check for correct PHP semantics. It should be included in every PHP based project.

- Template: `code_quality/phpstan.yml`
- Jobs: `.phpstan_phar`, `.phpstan_vendor`
- Variables: `PATHS_TO_SOURCES`, `PHPSTAN_VERSION`, `PATH_TO_COMPOSER`
- Configuration Files: `code_quality/config/phpstan.neon.dist`

You can choose between a phar-based version of the job that downloads a fresh copy of PHPStan PHAR (`.phpstan_phar`) and a vendor-based version of the job in case you have already included PHPStan in your project's vendors (`.phpstan_vendor`). Since phpstan will check the semantics of your code (e.g. if a class you refer to does actually exist), please make sure to have your vendors available when running the job (run `composer install` in the `before_script` or make use of caching).

The phar-based version of the job executes a `composer install` therefore it needs the path to the composer.phar. By default it is set to `/usr/local/bin/composer.phar`. To change this you can specify the path to composer.phar via the variable `PATH_TO_COMPOSER`.

The phar-based version uses the PHPStan version '0.12.58' by default. If you want to use a different version you can specify the version via the variable `PHPSTAN_VERSION` (e.g. '0.12.56').

Both versions require a `phpstan.neon` config file located at your project's root directory. The `phpstan.neon.dist` provided in the `config` folder will suit most projects, so just go ahead and copy it to your project's root directory for a start.

If you run your pipeline now, things should already work and print a report to the `stdout` of your job. That is, if all of your PHP source files are located at a directory called `src`. If all or some of your source files are located somewhere else, you can override the variable `PATHS_TO_SOURCES` in your job definition and list all directories to be checked seperated by a single space.


#### PHPMD - Mess Detector

This is a static analysis to discover messy bits of your code. It should be included in every PHP based project.

- Template: `code_quality/phpmd.yml`
- Jobs: `.phpmd_phar`, `.phpmd_vendor`
- Variables: `PATHS_TO_SOURCES`,`FORMAT`,`CONFIG_FILE`,`OUTPUT_FILE`,`PHPMD_VERSION` 
- Configuration Files: `code_quality/config/phpmd.xml.dist`

You can choose between a phar-based version of the job that downloads a fresh copy of PHPMD PHAR (`.phpmd_phar`) and a vendor-based version of the job in case you have already included PHPMD in your project's vendors (`.phpmd_vendor`). If using the vendor-based version, please make sure to have your vendors available when running the job (run `composer install` in the `before_script` or make use of caching).

The phar-based version uses the latest release of PHP Mess Detector by default. If you want to use a different version you can specify the version via the variable `PHPMD_VERSION` (e.g. '2.9.1').

Both versions require a `phpmd.xml` config file located at your project's root directory. The `phpmd.xml.dist` provided in the `config` folder will suit most projects, so just go ahead and copy it to your project's root directory for a start.

If you run your pipeline now, things should already work and there will a JSON file as artifacts available for download. If your source files are located somewhere else, you can override the variable `PATHS_TO_SOURCES` in your job definition.

Currently PHPMD does not support to analyse code in multiple places (separated by comma or space) like PHPStan or PHP CodeSniffer.


#### PhpMetrics

This is a static analysis that collects different metrics of the checked code and creates a nice overview.

- Template: `code_style/phpmetrics.yml`
- Jobs: `.phpmetrics_phar`
- Variables: `PHPMETRICS_VERSION`, `PHPMETRICS_CONFIG_PATH`, `PHPMETRICS_REPORT_PATH`
- Configuration File: `code_style/config/.phpmetrics.json.dist`

The job downloads the PhpMetrics PHAR. You can define a specific version by overriding the variable `PHPMETRICS_VERSION`. The job needs a configuration file in JSON format. You can start with the file `.phpmetrics.json.dist` that is provided in the `config` folder. Just copy it to your project's root directory and rename it to `.phpmetrics.json`. If your configuration file is located at a different path, you can define it by overriding the variable `PHPMETRICS_CONFIG_PATH`.

As a result, the job will create an HTML report at the path that is defined in the configuration file. By default, the path is just `phpmetrics-report/`. If you change that path in the configuration file, make sure to override the variable `PHPMETRICS_REPORT_PATH` to match it. The report will be provided as a GitLab artefact that you can download directly on the job page. Caution: the report will be deleted automatically after one day to prevent unnecessary data accumulations on the GitLab runner.

Further information is available at [PhpMetric's GitHub page](https://github.com/phpmetrics/PhpMetrics) and also specifically regarding [supported metrics](https://github.com/phpmetrics/PhpMetrics/blob/master/doc/metrics.md).

---

### Security

This section includes job templates to check your application for security vulnerabilities.

#### Local PHP Security Checker

This is a static analysis to check if your application uses dependencies with known security vulnerabilities.

- Template: `security/php-security-checker.yml`
- Job: `.php_security_check`
- Variables: `PATH_TO_COMPOSER_LOCK`

If you run your pipeline now, things should already work and print a report to the `stdout` of your job. That is, if your composer.lock file is located at your root directory. If the composer.lock file is located somewhere else, you can override the variable `PATH_TO_COMPOSER_LOCK` in your job definition.

#### Identify Outdated Dependencies (composer outdated)

This is a static analysis to check if your application uses outdated dependencies. The check is based on composer, so only dependencies managed by composer are considered here.

- Template: `security/composer_outdated.yml`
- Jobs: `.composer_outdated`
- Variables: `PATH_TO_COMPOSER`

Both jobs install your dependencies using composer first - if composer is located at a specific place for your project, you can change the path to it by overriding the variable `PATH_TO_COMPOSER`. 

The job`.composer_outdated` checks your direct dependencies for minor updates. It is meant to be included in your default pipeline, because [SemVer](https://semver.org/)-compatible minor updates of your direct dependencies should generally be done as quickly as possible, because no code adaptions should be expected (no BC-breaks).

The job `.composer_outdated_status` checks all of your project's dependencies (direct and indirect) for minor and major updates. Because major updates might require code adaptions, or you do not want to update them because you want to stick to LTS versions, this job is meant for a regular check in a scheduled pipeline to keep an overview on the status of your dependencies. You can refer to the [official GitLab Docs](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#configuring-pipeline-schedules) to learn how to set up a scheduled pipeline. For this job it might be enough if your scheduled pipeline runs once a week - but basically that depends on your project and what kind of update or maintenance strategy is set up.

---

### Testing

This section includes job templates, that execute automated tests of your application.

#### Unit Testing

In this section you will find templates for jobs that execute your unit tests. Of course you have to make sure yourself that your application contains enough unit tests.

##### Codeception

Codeception is a Testing Framework, that supports Unit Tests, Functional Tests and Acceptance Tests. It's used here to execute your unit tests.

- Template: `testing/unit/codeception.yml`
- Jobs: `.codeception_unit`, `.codeception_unit_coverage`
- Variables: `PATH_TO_TEST_OUTPUT`, `PATH_TO_COMPOSER`
- Configuration Files: `testing/unit/config/codeception.yml.dist`

As a pre-requisite to use these job templates, you need to have Codeception initialized in your project and have written a few unit tests. Here you can find the [Codeception Quickstart-Guide](https://codeception.com/quickstart), that helps you to set up Codeception in your project.

There is a basic job (`.codeception_unit`) that executes your unit tests and outputs a test report. Additionally, the test report gets integrated in GitLab as so-called Cobertura report, so you have a user friedly overview of your tests results. You can access this overview if you click on one of your pipelines and navigate to the tab "Tests" (Note: this tab will only be available after this job has been finished).

Another job (`.codeception_unit_coverage`) will also generate a code coverage report in addition to executing your unit tests. On the one hand you will see a text report as the job's output and on the other hand a so-called junit report gets integrated in GitLab so you can e.g. see changes of your code coverage in merge requests.

Because codeception needs to be integrated in your project as a vendor, both jobs run `compoer install` first. If necessary, you can change the path to composer by overriding the variable `PATH_TO_COMPOSER` in your job definition. By default, the path is set to `/usr/local/bin/composer.phar`.

Furthermore, both jobs access the output that is generated by Codeception. This output is located at `tests/_output` by default - if that is not the case for you, you can override the variable `PATH_TO_TEST_OUTPUT` with the path that suits your project.

The required configuration file (`codeception.yml`) will be generated when initializing Codeception. The dist-file provided here can be checked as a reference. It is important that you choose the included paths for code coverage suitable for your project.

---

### Release

This section contains job templates that perform tasks during the release process of your application.

#### Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) is a tool for complete automation of version management based on [Semantic Versioning](https://semver.org/). 

- Template: `release/semantic-release.yml`
- Jobs: `.semantic_release`
- Variables: `SEM_REL_OPTIONS`
- Configuration Files: `release/config/.releaserc.json`, `release/config/.releaserc-master.json`

To use this job (`.semantic_release`), first of all you need to include it in your `.gitlab-ci.yml` and assign it to a stage. Attention: this job requires a docker runner, so you need to tag it accordingly. For SkyGate's internal GitLab, the tag is simply called `docker`. You can just copy the configuration file `release/config/.releaserc.json` into your project's root directory, if your main branch is already called "main" instead of "master". If your project is still running on "master" just use the configuration file `release/config/.releaserc-master.json` instead and remove the "-master" from the file name. 

To make sure the job is allowed to push changes to your repository, you need to create a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) at Settings -> Access Tokens that has the permissions `api` and `write_repository` and the role `Maintainer`. You can choose any name you like for the token. You will need the value of the token for the next step: now you need to create a new variable named `GITLAB_TOKEN` at Settings -> CI/CD -> Variables and set the token value you have just copied for the variable's value.

When you push anything to "main" ("master") now, a release job will run in your pipeline. __Before you do that, please make sure to tag your latest commit with the current version (use v1.0.0 if you did not have any tags before).__

You will find further information in the [SkyGate-Wiki](https://wiki.int.skygate.de/display/SGDEV/Automatisierte+Releases+mit+semantic-release) (german).
