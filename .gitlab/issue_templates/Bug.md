# Zusammenfassung / Beschreibung

## Wie stellt sich das derzeitige fehlerhafte Verhalten dar?

## Wie wäre das erwartete richtige Verhalten?

## Betroffene(s) CI Template(s)

* [ ] code_style/code_sniffer_phar
* [ ] code_style/code_sniffer_vendor
* [ ] code_style/php_compat_phar
* [ ] code_style/php_compat_vendor
* [ ] code_quality/phpstan_phar
* [ ] code_quality/phpstan_phar_install
* [ ] code_quality/phpstan_vendor
* [ ] code_quality/phpmetrics_phar
* [ ] code_quality/phpmd_phar
* [ ] code_quality/phpmd_vendor
* [ ] security/composer_outdated
* [ ] security/composer_outdated_status
* [ ] security/php_security_check
* [ ] testing/unit/codeception_unit
* [ ] testing/unit/codeception_unit_coverage
* [ ] testing/unit/unit_tests

## Wie sieht die .gitlab-ci.yml des betroffenen Projekts aus?

## Wie sieht der Output des/der betroffenen Jobs aus?
