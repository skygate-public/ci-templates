#!/bin/bash

if [ -z "$ROCKET_CHAT_URL" ] || [ -z "$ROCKET_CHAT_CHANNEL" ]; then
  echo "Please define ROCKET_CHAT_URL and ROCKET_CHAT_CHANNEL in your project's ci variables."
  exit 0;
fi

timestamp() {
    date +"%Y-%m-%d %H:%M:%S"
}

TITLE="Deployed $CI_PROJECT_NAME:$CI_COMMIT_REF_NAME on $CI_ENVIRONMENT_NAME"
MSG="Deployment successfully finished at $(timestamp)"

INFO="Branch: $CI_COMMIT_REF_NAME, Commit-Hash: $CI_COMMIT_SHA"

PAYLOAD="{\"channel\": \"$ROCKET_CHAT_CHANNEL\", \"status\": \"success\", \"title\": \"$TITLE\", \"message\": \"$MSG\", \"context\": {\"server\": \"$CI_ENVIRONMENT_URL\", \"application\": \"$CI_PROJECT_TITLE\", \"information\": \"$INFO\"}}"

curl -vvv "$ROCKET_CHAT_URL" -d "payload=$PAYLOAD"