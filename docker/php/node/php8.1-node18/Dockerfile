FROM node:18-alpine AS node
FROM php:8.1-cli-alpine

RUN apk update \
    && apk upgrade \
    && apk add --no-cache \
    make \
    rsync \
    git \
    curl \
    openssh-client \
    bash \
    python3 \
    freetype-dev \
    libpng-dev \
    jpeg-dev \
    libjpeg-turbo-dev \
    gd \
    libzip-dev \
    zip \
    g++

RUN docker-php-ext-install gd zip sockets

COPY --from=composer:2.2 /usr/bin/composer /usr/local/bin/composer

COPY --from=node /usr/lib /usr/lib
COPY --from=node /usr/local/share /usr/local/share
COPY --from=node /usr/local/lib /usr/local/lib
COPY --from=node /usr/local/include /usr/local/include
COPY --from=node /usr/local/bin /usr/local/bin

RUN npm install -g eslint@8.57.0
