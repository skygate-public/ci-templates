# Verfügbare Security Job-Templates

Dieser Bereich enthält Templates für Jobs, die deine Applikation auf Sicherheitslücken hin untersucht.

## Local PHP Security Checker

Dies ist eine statische Analyse, die überprüft, ob deine Applikation Abhängigkeiten nutzt, die bekannte Sicherheitslücken haben.

- Template: `security/php-security-checker.yml`
- Job: `.php_security_check`
- Variablen: `PATH_TO_COMPOSER_LOCK`

Wenn du deine Pipeline jetzt ausführst, sollte der Job bereits funktionieren und einen Bericht auf der `stdout` deines Jobs ausgeben. Zumindest, wenn deine composer.lock Datei im Hauptverzeichnis deines Projektes liegt. Falls sie sich an einem anderen Ort befindet, kannst du die Variable `PATH_TO_COMPOSER_LOCK` in deiner Job-Definition überschreiben.

## SBOM und Dependency Track

SkyGate setzt zur Verwaltung von Abhängigkeiten das [Tool Dependency](https://deptrack.int.skygate.de/) Track ein. Hier werden zu jedem Projekt alle enthaltenen Abhängigkeiten in den genutzten Versionen hinterlegt, um diese kontinuierlich auf Sicherheitslücken zu prüfen.

Die gannten Informationen über Projekt-Abhängigkeiten werden für jedes Projekt in der CI-Pipeline ermittelt und in Form eines sogenannten SBOMs (Software Bill of Materials) automatisiert an Dependency Track übermittelt und zwar immer dann, wenn eine neue Version des Projektes erstellt ("getaggt") wird.

Dazu muss zunächst ein Job der Pipeline hinzugefügt werden, der das SBOM erstellt. Dafür stehen zwei Templates zur Verfügung: eines, dass das SBOM direkt basierend auf der Projektstruktur erstellt und eines, dass das SBOM auf Basis eines vom Projekt gebuildeten Docker Images erstellt - je nachdem, wie das Projekt ausgeliefert wird.

### SBOM basierend auf der Projektstruktur

- Template: `security/dependency_track.yml`
- Job: `.project_sbom`
- Variablen: `SBOM_JSON_PATH`, `PATH_TO_PROJECT`

Standardmäßig wird eine JSON-Datei mit dem Namen sbom.json ausgehend vom aktuellen Verzeichnis erzeugt und als Artefakt gespeichert.

### SBOM basierend auf Docker Image

- Template: `security/dependency_track.yml`
- Job: `.image_sbom`
- Variablen: `SBOM_JSON_PATH`

Standardmäßig wird eine JSON-Datei mit dem Namen sbom.json ausgehend vom ebenfalls in der Pipeline gebuildeten Image erzeugt.

### Übermittlung an Dependency Track

- Template: `security/dependency_track.yml`
- Job: `.dependency_track`
- Variablen: `SBOM_JSON_PATH`

Dieser Job nimmt das im vorherigen Job erzeugte SBOM (standardmäßig "sbom.json") und übermittelt es an Dependency Track. Dazu muss in den CI Variablen der $DEPTRACK_API_KEY sowie die $DEPTRACK_PROJECT_ID spezifiziert werden. Beide Informationen erhält man auf Nachfrage von der Systemadministration, sobald das entsprechende Projekt von der Systemadministration in Depedency Track erstellt und konfiguriret worden ist.

## Veraltete Abhängigkeiten identifizieren (composer outdated)

Dies ist eine statische Analyse, die überprüft, ob deine Applikation Abhängigkeiten nutzt, die veraltet sind. Die Überprüfung basiert auf composer, daher können hier nur Abhängigkeiten berücksichtigt werden, die per composer verwaltet werden.

- Template: `security/composer_outdated.yml`
- Jobs: `.composer_outdated`
- Variablen: `PATH_TO_COMPOSER`

Beide Jobs installieren zunächst alle deine Abhängigkeiten per composer install - falls composer bei dir an einem besonderen Ort liegt, kannst du den Pfad durch Überschreiben der Variable `PATH_TO_COMPOSER` an deine Bedürfnisse anpassen. 

Der Job `.composer_outdated` überprüft die direkten Projekt-Dependencies auf vorhandene Minor-Updates. Er ist dazu gedacht, direkt in deine Standard-Pipeline eingebunden zu werden, denn [SemVer](https://semver.org/)-kompatible Minor-Updates deiner direkten Dependencies solltest du grundsätzlich so zeitnah wie möglich durchführen, da keine Notwendigkeit für Code-Anpassungen bestehen sollte (keine BC-Breaks).

Der Job `.composer_outdated_status` überprüft sämtliche Projekt-Dependencies (direkte und indirekte) auf Minor- und Major-Updates. Da Major-Updates auch Code-Anpassungen notwendig machen können, oder man vor dem Hintergrund von LTS-Versionen noch nicht auf die nächste Major-Version wechseln will, ist dieser Job für die regelmäßige Ausführung innerhalb einer Scheduled Pipeline ausgelegt, um den Überblick zu behalten. Wie du eine Scheduled Pipeline einrichtest, erfährst du in der [offiziellen GitLab-Doku](https://docs.gitlab.com/ee/ci/pipelines/schedules.html#configuring-pipeline-schedules). Für diesen Job kann es reichen, deine Scheduled Pipeline wöchentlich laufen zu lassen - das ist aber im Grunde von deinem Projekt und der dort vereinbarten Update- bzw. Wartungsstrategie abhängig. 
