_Dokumentation in [englischer Sprache](../README.en.md) lesen_

# SkyGate CI Template Repository

Dieses Repository stellt CI templates bereit, die von SkyGate-Entwicklern sowohl für SkyGate-interne Projekte als auch für Kundenprojekte genutzt werden können. 

Die Templates können ganz einfach per Template Include aus anderen __SkyGate GitLab__ Repositories heraus referenziert werden. 

Repositories, die auf __gitlab.com__ gehosted sind, können die Templates ebenfalls per Template Include einbinden - dafür steht ein gespiegeltes, öffentliches Repository unter https://gitlab.com/skygate-public/ci-templates/ bereit.

Aus anderen externen Repositories heraus können die Templates durch Kopie genutzt werden.

## Struktur dieses Repositories

Die Templates sind in Ordnern nach Thema organisiert. Alle Konfigurationsdateien, die für das Ausführen eines Jobs benötigt werden, befinden sich in einem Unterordner names `config` innerhalb des jeweiligen Themen-Ordners. 

## Nutzung eines Job Template per Template Include (für Repositorys im SkyGate GitLab oder auf gitlab.com)

Zuerst benötigst du in deinem Projekt eine Konfigurationsdatei für deine GitLab CI pipeline (`.gitlab-ci.yml`). Solltest du also noch keine haben, erstelle sie jetzt. Gehen wir davon aus, dass du den Job `sample-job` nutzen möchtest, der in der Datei `code_quality/example.yml` definiert ist. 

1. Referenziere das Template, in dem der Job sich befindet, via Template Include in deiner `.gitlab-ci.yml`:

       // Repository im SkyGate GitLab:
       include:
          - project: 'skygate/ci-templates'
            ref: 'master'
            file: 'code_quality/example.yml'
            
       // Repository auf gitlab.com:
       include:
          - project: 'skygate-public/ci-templates'
            ref: 'master'
            file: 'code_quality/example.yml'

2. Definiere einen Job, der sich mittels `extends` auf den Job aus dem Template bezieht, um ihn einer Stage zuzuweisen:

       my-project-job:
          extends: .sample-job
          stage: my-stage

3. (Optional) Überschreibe bei Bedarf Variablen, die der Job aus dem Template definiert:

       my-project-job:
          ...
          variables:
              JOB_VARIABLE: 'my custom value'


### Nutzung einer spezifischen Version eines Job-Templates

Sollte dein Projekt eine ganz bestimmte Version eines Templates benötigen, kannst du diese mithilfe des 'ref'-Attributs definieren. 

Beide Templates aus dem `master` verwenden:

       include:
           - project: 'skygate/ci-templates'
             ref: 'master'
             file:
               - 'code_quality/example_1.yml'
               - 'code_quality/example_2.yml'

Verschiedene Versionen verwenden:

       include:
           - project: 'skygate/ci-templates'
             ref: '1.2.0'
             file: 'code_quality/example_1.yml'
           - project: 'skygate/ci-templates'
             ref: '2.0.0'
             file: 'code_quality/example_2.yml'

## Verfügbare Job-Templates

### Code Style

Dieser Bereich enthält Templates für Jobs, die die korrekte Code-Syntax überprüfen und sicherstellen, dass verschiedene Coding-Standards eingehalten werden. 

#### PHP CodeSniffer

Dies ist eine sehr grundlegende Überprüfung auf korrekte PHP-Syntax und Einhaltung des Coding-Standards PSR-12. Es sollte in jedem PHP-basierten Projekt verwendet werden.

- Template: `code_style/code-sniffer.yml`
- Jobs: `.code_sniffer_phar`, `.code_sniffer_vendor`
- Variablen: `PATHS_TO_SOURCES`, `PHPCS_VERSION`, `PATH_TO_CACHE`
- Konfigurationsdatei: `code_style/config/phpcs.xml.dist`

Du kannst dich zwischen einer PHAR-basierten Version und einer vendor-basierten Version des Jobs entscheiden. Die PHAR-basierte Version (`.code_sniffer_phar`) lädt das PHP CodeSniffer PHAR herunter. Die vendor-basierte Version (`.code_sniffer_vendor`) kannst du nutzen, falls du CodeSniffer eh schon in deinem Projekt eingebunden hast. Wenn du die vendor-basierte Version benutzt, stelle bitte sicher, dass deine vendors auch zur Verfügung stehen, wenn der Job ausgeführt wird (z.B. `composer install` im `before_script` ausführen oder Caching verwenden).

Die PHAR-basierte Version nutzt standardmäßig das aktuellste Release von PHP CodeSniffer. Falls du eine andere Version benötigst, kannst du diese durch Überschreiben der Variable `PHPCS_VERSION` angeben (z.B. '3.5.8').

Beide Versionen benötigen eine `phpcs.xml` Konfigurationsdatei im Hauptverzeichnis deines Projektes. Die im `config`-Ordner zur Verfügung gestellte `phpcs.xml.dist` passt für die meisten Projekte, also kopiere sie für den Anfang einfach in das Hauptverzeichnis deines Projektes.

Wenn du deine Pipeline jetzt ausführst, sollte der Job bereits funktionieren und einen Bericht auf der `stdout` deines Jobs ausgeben. Zumindest, wenn alle deine Quelldateien sich in einem Ordner namens `src` befinden. Falls alle oder einige deiner Quelldateien an einem anderen Ort liegen, kannst du die Variable `PATHS_TO_SOURCES` in deiner Job-Definition überschreiben und alle Ordner mit Komma getrennt auflisten, in denen der Code gecheckt werden soll.

CodeSniffer verwendet einen Cache um die Analyse zu beschleunigen. Du kannst den Pfad zum Cache-Verzeichnis setzten, indem Du die Variable `PATH_TO_CACHE` überschreibst. Der Standard-Wert ist `var/.phpcs-cache`. Du solltest sicherstellen, dass der Pfad existiert.

Hinweis für Symfony-basierte Projekte: Da der Standard-Cache-Pfad in `var` liegt, dieses Verzeichnis jedoch standardmäßig ignoriert wird, muss hier folgendes in der `.gitignore` ergänzt werden:

    !var/ 
    var/*
    !var/.gitkeep

Zusätzlich muss die `.gitkeep` Datei im `var`-Verzeichnis angelegt werden.

#### PHP Compatibility Check

Mithilfe dieses Checks wird der Code auf Kompatibilität zu einer bestimmten PHP Version überprüft. Er sollte in jedem Projekt einsetzt werden, sobald ein Update auf eine höhere PHP Version geplant ist. 

- Template: `code_style/php-compat.yml`
- Jobs: `.php_compat_phar`, `.php_compat_vendor`
- Variablen: `PATHS_TO_SOURCES`, `PHPCS_VERSION`, `PATH_TO_CACHE`, `PHP_VERSION`
- Konfigurationsdatei: `code_style/config/phpcs.xml.dist`

Du kannst dich auch hier zwischen einer PHAR-basierten Version und einer vendor-basierten Version des Jobs entscheiden. Die Jobs basieren auf PHP CodeSniffer, d.h. sämtliche Konfigurationsmöglichkeiten, die dir für "PHP CodeSniffer" (s.o.) zur Verfügung stehen, kannst du hier auch nutzen. 

Bei der PHAR-basierten Job-Version (`.php_compat_phar`) wird neben CodeSniffer selbst auch der benötigte Standard "PHPCompatiblity" heruntergeladen. 

Die vendor-basierte Version (`.php_compat_vendor`) setzt voraus, dass dieser Standard bereits in deinem Projekt über die vendors installiert und registriert ist. Infos dazu findest du in der [PHP Compatibility Doku](https://github.com/PHPCompatibility/PHPCompatibility#installation-in-a-composer-project-method-1).

Die PHP Version, auf deren Kompatibilität dein Code überprüft wird, ist standardmäßig auf PHP 8.0 gesetzt. Wenn du auf eine andere Version checken möchstest, kannst du diese durch Überschreiben der Variable `PHP_VERSION` definieren.

---

### Code Quality

Dieser Bereich enthät Templates für Jobs, die die Code-Qualiät überprüfen. Das beinhaltet die semantische Überprüfung, Bugs, suboptimal gestalteter Code, ungenutzte Parameter, Methoden und Eigenschaften, etc. 

#### PHPStan

Dies ist eine statische Analyse zur Überprüfung der korrekten Semantik des PHP-Codes. Es sollte in jedem PHP-basierten Projekt verwendet werden.

- Template: `code_quality/phpstan.yml`
- Jobs: `.phpstan_phar`, `.phpstan_vendor`
- Variablen: `PATHS_TO_SOURCES`, `PHPSTAN_VERSION`, `PATH_TO_COMPOSER`
- Konfigurationsdatei: `code_quality/config/phpstan.neon.dist`

Du kannst dich zwischen einer PHAR-basierten Version und einer vendor-basierten Version des Jobs entscheiden. Die PHAR-basierte Version (`.phpstan_phar`) lädt das PHPStan PHAR herunter. Die vendor-basierte Version (`.phpstan_vendor`) kannst du nutzen, falls du PHPStan eh schon in deinem Projekt eingebunden hast. Wenn du die vendor-basierte Version benutzt, stelle bitte sicher, dass deine vendors auch zur Verfügung stehen, wenn der Job ausgeführt wird (z.B. `composer install` im `before_script` ausführen oder Caching verwenden).

Die PHAR-basierte Version des Jobs führt ein `composer install` aus und benötigt daher den Pfad zum composer.phar. Standardmäßig ist dieser auf `/usr/local/bin/composer.phar` gesetzt. Um den Pfad zu ändern, kannst du die Variable `PATH_TO_COMPOSER` in deiner Job-Definition überschreiben.

Die PHAR-basierte Version nutzt standardmäßig die Version '0.12.58' von PHPStan. Falls du eine andere Version benötigst, kannst du diese durch Überschreiben der Variable `PHPSTAN_VERSION` angeben (z.B. '0.12.56').

Beide Versionen benötigen eine `phpstan.neon` Konfigurationsdatei im Hauptverzeichnis deines Projektes. Die im `config`-Ordner zur Verfügung gestellte `phpstan.neon.dist` passt für die meisten Projekte, also kopiere sie für den Anfang einfach in das Hauptverzeichnis deines Projektes.

Wenn du deine Pipeline jetzt ausführst, sollte der Job bereits funktionieren und einen Bericht auf der `stdout` deines Jobs ausgeben. Zumindest, wenn alle deine Quelldateien sich in einem Ordner namens `src` befinden. Falls alle oder einige deiner Quelldateien an einem anderen Ort liegen, kannst du die Variable `PATHS_TO_SOURCES` in deiner Job-Definition überschreiben und alle Ordner mit Komma getrennt auflisten, in denen der Code gecheckt werden soll.


#### PHPMD - Mess Detector

Dies ist eine statische Analyse, um problematische Code-Stellen zu identifizieren. Es sollte in jedem PHP-basierten Projekt verwendet werden.

- Template: `code_quality/phpmd.yml`
- Jobs: `.phpmd_phar`, `.phpmd_vendor`
- Variablen: `PATHS_TO_SOURCES`,`FORMAT`,`CONFIG_FILE`,`OUTPUT_FILE`,`PHPMD_VERSION` 
- Konfigurationsdatei: `code_quality/config/phpmd.xml.dist`

Du kannst dich zwischen einer PHAR-basierten Version und einer vendor-basierten Version des Jobs entscheiden. Die PHAR-basierte Version (`.phpmd_phar`) lädt das PHPMD PHAR herunter. Die vendor-basierte Version (`.phpmd_vendor`) kannst du nutzen, falls du PHP Mess Detector eh schon in deinem Projekt eingebunden hast. Wenn du die vendor-basierte Version benutzt, stelle bitte sicher, dass deine vendors auch zur Verfügung stehen, wenn der Job ausgeführt wird (z.B. `composer install` im `before_script` ausführen oder Caching verwenden).

Die PHAR-basierte Version nutzt standardmäßig das aktuellste Release von PHP Mess Detector. Falls du eine andere Version benötigst, kannst du diese durch Überschreiben der Variable `PHPMD_VERSION` angeben (z.B. '2.9.1').

Beide Versionen benötigen eine `phpmd.xml` Konfigurationsdatei im Hauptverzeichnis deines Projektes. Die im `config`-Ordner zur Verfügung gestellte `phpmd.xml.dist` passt für die meisten Projekte, also kopiere sie für den Anfang einfach in das Hauptverzeichnis deines Projektes.

Wenn du deine Pipeline jetzt ausführst, sollte der Job bereits funktionieren und einen Bericht auf der `stdout` deines Jobs ausgeben. Zumindest, wenn alle deine Quelldateien sich in einem Ordner namens `src` befinden. Falls deine Quelldateien an einem anderen Ort liegen, kannst du die Variable `PATHS_TO_SOURCES` in deiner Job-Definition überschreiben.

PHP Mess Detector unterstützt derzeit noch keine Quelldateien an mehreren Orten (gentrennt durch Komma oder Leerzeichen) wie PHPStan oder PHP CodeSniffer.


#### PhpMetrics

Dies ist eine umfangreiche statische Analyse, die diverese Metriken über den überprüften Code erstellt und übersichtlich aufbereitet.

- Template: `code_style/phpmetrics.yml`
- Jobs: `.phpmetrics_phar`
- Variablen: `PHPMETRICS_VERSION`, `PHPMETRICS_CONFIG_PATH`, `PHPMETRICS_REPORT_PATH`
- Konfigurationsdatei: `code_style/config/.phpmetrics.json.dist`

Der Job lädt das PhpMetrics PHAR herunter. Eine bestimmte Version kannst du nutzen, indem du diese durch Überschreiben der Variable `PHPMETRICS_VERSION` definierst. Der Job benötigt eine Konfigurationsdatei im JSON-Format. Du kannst mit der im `config`-Ordner zur Verfügung gestellten Datei `.phpmetrics.json.dist` starten, indem du sie einfach in das Hauptverzeichnis deines Projektes kopierst und in `.phpmetrics.json` umbenennst. Sollte deine Konfigurationsdatei unter einem anderen Pfad liegen, kannst du diesen durch Überschreiben der Variable `PHPMETRICS_CONFIG_PATH` definieren.

Als Ergebnis erzeugt der Job einen HTML-Bericht, der unter dem in der Konfigurationsdatei definierten Pfad generiert wird. Standardmäßig ist dieser Pfad `phpmetrics-report/`. Wenn du ihn in der Konfigurationsdatei anpasst, musst du auch die Variable `PHPMETRICS_REPORT_PATH` mit dem entsprechenden Pfad überschreiben. Der Bericht wird als GitLab Artefakt bereitgestellt, das du direkt am Job herunterladen kannst. Achtung: der Bericht wird nach einem Tag automatisch gelöscht, um unnötigen Anhäufungen von Datenmengen auf dem GitLab Runner vorzubeugen.

Weitere Informationen gibt es auf der [PhpMetrics GitHub Seite](https://github.com/phpmetrics/PhpMetrics) bzw. dort auch speziell zu den [unterstützten Metriken](https://github.com/phpmetrics/PhpMetrics/blob/master/doc/metrics.md).

---

### Security

siehe security/README.md 

---

### Testing

Dieser Bereich enthält Templates für Jobs, die automatisierte Tests deiner Applikation duchführen.

#### Unit Testing

In diesem Bereich findest du Templates für Jobs, die deine Unit Tests ausführen. Natürlich musst du selbst dafür sorgen, dass deine Applikation über ausreichend Unit Tests verfügt.

##### Codeception

Codeception ist ein Test-Framework, das neben Unit Tests auch Functional und Acceptance Tests untertützt. Hier wird es benutzt, um deine Unit Tests auszuführen.

- Template: `testing/unit/codeception.yml`
- Jobs: `.codeception_unit`, `.codeception_unit_coverage`
- Variablen: `PATH_TO_TEST_OUTPUT`, `PATH_TO_COMPOSER`
- Konfigurationsdatei: `testing/unit/config/codeception.yml.dist`

Voraussetzung für die Nutzung dieser Job-Templates ist, dass du Codeception in deinem Projekt initialisiert hast und erste Unit Tests geschrieben hast. Hier findest du den [Codeception Quickstart-Guide](https://codeception.com/quickstart), der dir dabei weiterhilft.

Es gibt einen Basis-Job (`.codeception_unit`), der deine Unit Tests ausführt und dir einen Test-Bericht ausgibt. Außerdem wird der Test-Bericht als sog. Cobertura-Report in GitLab integriert, so dass du eine nutzerfreundliche Übersicht deiner Testergebnisse erhälst. Zu dieser Übersicht gelangst du, wenn du auf eine deiner Pipelines klickst und dort zum Tab "Tests" navigierst (Achtung: der Tab steht erst zur Verfügung, nachdem dieser Job abgeschlossen wurde).

Ein weiterer Job (`.codeception_unit_coverage`) generiert zusätzlich zur Ausführung der Unit Tests auch einen Code-Coverage-Report. Zum einen siehst du diesen als Ausgabe des Jobs in textlicher Form und zum anderen wird er als sog. junit-Report in GitLab integriert, so dass du z.B. in Merge-Requests Veränderungen der Code-Coverage sehen kannst.

Da Codeception als Vendor in deinem Projekt integriert sein muss, führen beide Jobs zunächst ein `compoer install` aus. Den Pfad zu Composer kannst du falls nötig ändern, indem du die Variable `PATH_TO_COMPOSER` in deiner Job-Definition überschreibst. Standardmäßig ist der Pfad auf `/usr/local/bin/composer.phar` gesetzt.

Außerdem greifen beide Jobs auf den von Codeception erzeugten Output zu. Dieser liegt standardmäßig unter `tests/_output` - sollte das bei dir anders sein, kannst du die Variable `PATH_TO_TEST_OUTPUT` mit deinem projektspezifischen Pfad überschreiben.

Die benötigte Konfigurationsdatei (`codeception.yml`) wird beim Initialisieren von Codeception erzeugt. Die hier beiliegende dist-Datei kann als Referenz dienen - wichtig ist, dass du die enthaltenen Pfade für die Code Coverage für dein Projekt sinnvoll wählst.

---

### Release

Dieser Bereich enthält Templates für Jobs, die Aufgaben innerhalb des Release-Prozesses übernehmen.

#### Semantic Release

[Semantic Release](https://semantic-release.gitbook.io/semantic-release/) ist ein Tool zur vollständigen Automatisierung der Versionsverwaltung nach [Semantic Versioning](https://semver.org/). 

- Template: `release/semantic-release.yml`
- Jobs: `.semantic_release`
- Variablen: `SEM_REL_OPTIONS`
- Konfigurationsdatei: `release/config/.releaserc.json`, `release/config/.releaserc-master.json`

Um diesen (`.semantic_release`) Job nutzen zu können, musst du ihn zunächst in deiner `.gitlab-ci.yml` einbinden. Achtung: dieser Job benötigt einen Docker-Runner, du musst ihn also entsprechend taggen. Im internen SkyGate GitLab heißt das entsprechende Tag schlicht `docker`. In das Hauptverzeichnis deines Projektes kopierst du die Konfigurationsdatei `release/config/.releaserc.json`, wenn dein Haupt-Branch bereits "main" und nicht mehr "master" heißt. Bist du noch auf dem "master" unterwegs, kopierst du dir einfach die Datei `release/config/.releaserc-master.json` in dein Hauptverzeichnis und löschst das "-master" aus dem Dateinamen. 

Damit der Job in dein Repository pushen kann, musst du nun unter Settings -> Access Tokens ein neues [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) erzeugen, dass die Berechtigungen `api` und `write_repository` sowie die Rolle `Maintainer` besitzt. Den Namen kannst du frei wählen. Den Wert des Tokens brauchst du im nächsten Schritt: jetzt legst du unter Settings -> CI/CD -> Variables eine neue Variable namens `GITLAB_TOKEN` an und fügst als Wert den Wert deines gerade erstellten Tokens ein.

Wenn du nun in den "main" ("master") Branch pushst, enthält deine Pipeline einen Release-Job. __Bevor du das tust, stelle bitte unbedingt sicher, dass dein letzter Commit mit der aktuellen Version getaggt ist (nutze einfach v1.0.0, wenn du vorher noch keine Tags verwendet hast).__

Weitere Informationen findest du im [SkyGate-Wiki](https://wiki.int.skygate.de/display/SGDEV/Automatisierte+Releases+mit+semantic-release).
